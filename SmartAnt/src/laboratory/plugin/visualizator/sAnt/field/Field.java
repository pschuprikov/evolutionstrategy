package laboratory.plugin.visualizator.sAnt.field;

import javax.swing.*;
import java.awt.*;

import laboratory.plugin.visualizator.sAnt.automaton.DocumentPanel;
import laboratory.plugin.visualizator.sAnt.runner.AutomatonRunner;
import laboratory.plugin.task.ant.individual.Automaton;

public class Field extends JComponent {

	private MPanel main;
	private JPanel p;
	private DocumentPanel graph;
	private DocumentPanel nestedGraph;
	private ButtonPanel button;

	private double tableSize;
	private int eleven;

	public Field(String name, Automaton a, int width, int height,
			double tableSize, int fieldSize, int eleven) {

		setLayout(new BorderLayout());

		this.tableSize = tableSize;
		this.eleven = eleven;

		setSize(new Dimension(width, height));
		setMinimumSize(new Dimension(width, height));
		setMaximumSize(new Dimension(width, height));

		AutomatonRunner m = new AutomatonRunner(a);

		main = new MPanel(m, fieldSize, (int) (width - width * tableSize),
				height - 2 * eleven);
		add(main, BorderLayout.WEST);

		p = new JPanel();
		p.setLayout(new BorderLayout());

		graph = new DocumentPanel(true, a, m, (int) (width * tableSize)
				- eleven, height / 2 - 3 * eleven);
		nestedGraph = new DocumentPanel(true, a.getNestedAutomaton(),
				m.getNestedMover(), (int) (width * tableSize) - eleven, height
						/ 2 - 3 * eleven);

		p.add(graph, BorderLayout.NORTH);
		p.add(nestedGraph, BorderLayout.SOUTH);

		add(p, BorderLayout.EAST);

		MPanel[] temp = { main };
		DocumentPanel[] tmp = { graph, nestedGraph };

		button = new ButtonPanel(temp, tmp);

		button.setPreferredSize(new Dimension(getWidth(), 30));

		add(button, BorderLayout.SOUTH);
	}

	public void paint(Graphics g) {
		button.setPreferredSize(new Dimension(getWidth(), 30));
		button.setSize(new Dimension(getWidth(), 30));
		main.setPreferredSize(new Dimension((int) (getWidth() * tableSize
				- eleven + 3), getHeight() - button.getHeight() - eleven));
		// main.setSize(new Dimension((int)(getWidth() * TABLE_SIZE),
		// getHeight() - button.getHeight()));
		p.setPreferredSize((new Dimension(
				(int) (getWidth() * (1 - tableSize) - eleven), getHeight()
						- button.getHeight() - eleven)));
		// p.setSize((new Dimension((int)(getWidth() * (1 - TABLE_SIZE)),
		// getHeight() - button.getHeight())));
		graph.setPreferredSize(new Dimension((int) p.getPreferredSize()
				.getWidth() - eleven, (int) p.getPreferredSize().getHeight()
				/ 2 - eleven));
		// graph.setSize(new Dimension((int)p.getPreferredSize().getWidth(),
		// (int)p.getPreferredSize().getHeight() / 2));
		nestedGraph.setPreferredSize(graph.getPreferredSize());
		// nestedGraph.setSize(graph.getPreferredSize());
		super.paint(g);
	}
}
