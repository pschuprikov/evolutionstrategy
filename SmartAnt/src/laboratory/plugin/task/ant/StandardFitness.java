package laboratory.plugin.task.ant;

import java.util.List;

import laboratory.plugin.task.ant.individual.Automaton;
import laboratory.plugin.task.ant.individual.SimpleMover;

import org.uncommons.watchmaker.framework.FitnessEvaluator;

public class StandardFitness<A extends Automaton> implements
		FitnessEvaluator<A> {

	public double calk(Mover solution) {
		solution.restart(new SimpleAnt());
		int count = 0;
		int lem = 0;
		for (int i = 0; i < Ant.NUMBER_STEPS; i++) {
			if (solution.move()) {
				count++;
				lem = i;
			}
			if (count == Ant.NUMBER_FOOD) {
				break;
			}
		}
		return (count - lem * 1.0 / Ant.NUMBER_STEPS);
	}

	@Override
	public double getFitness(A candidate, List<? extends A> population) {
		return calk(new SimpleMover(candidate));
	}

	@Override
	public boolean isNatural() {
		return true;
	}
}
