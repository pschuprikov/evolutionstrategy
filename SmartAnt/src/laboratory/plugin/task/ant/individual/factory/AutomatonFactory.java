package laboratory.plugin.task.ant.individual.factory;

import laboratory.plugin.task.ant.individual.Automaton;

import java.util.Random;

import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;

public abstract class AutomatonFactory<A extends Automaton> extends
		AbstractCandidateFactory<A> {

	private final int numberStates;
	private final int numberNestedStates;

	public AutomatonFactory(int ns, int nns) {
		numberStates = ns;
		numberNestedStates = nns;
	}

	protected abstract A fullRandomAutomaton(int ns, Random r);

	protected abstract A randomAutomatonWN(int ns, A na, Random r);

	public A generateRandomCandidate(Random rng) {
		if (numberNestedStates != 0) {
			return randomAutomatonWN(numberStates,
					fullRandomAutomaton(numberNestedStates, rng), rng);
		} else {
			return fullRandomAutomaton(numberStates, rng);
		}
	}
}
