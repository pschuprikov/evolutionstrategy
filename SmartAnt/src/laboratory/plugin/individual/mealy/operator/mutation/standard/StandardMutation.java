package laboratory.plugin.individual.mealy.operator.mutation.standard;

import laboratory.plugin.task.ant.individual.operator.AbstractAutomatonMutation;
import laboratory.plugin.individual.mealy.MealyAutomaton;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.uncommons.watchmaker.framework.EvolutionaryOperator;

public class StandardMutation extends AbstractAutomatonMutation<MealyAutomaton>
		implements EvolutionaryOperator<MealyAutomaton> {

	@Override
	public List<MealyAutomaton> apply(List<MealyAutomaton> selectedCandidates,
			Random rng) {
		ArrayList<MealyAutomaton> mutated = new ArrayList<MealyAutomaton>();
		for (MealyAutomaton ma : selectedCandidates) {
			mutated.add(apply(ma, rng));
		}
		return mutated;
	}
}