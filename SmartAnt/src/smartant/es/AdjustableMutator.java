package smartant.es;

import org.uncommons.watchmaker.framework.EvolutionaryOperator;

public interface AdjustableMutator<T> extends EvolutionaryOperator<T>  {
	public void increaseMutation();
	public void decreaseMutation();
}
