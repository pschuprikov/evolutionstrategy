package smartant.es;

import org.uncommons.watchmaker.framework.PopulationData;
import org.uncommons.watchmaker.framework.TerminationCondition;

public class EvolutionStrategyTerminationCondition implements
		TerminationCondition {

	private final int restartNumber;
	private int maxCnt = 0;
	private double bestFitness = 0;

	public EvolutionStrategyTerminationCondition(int restartNumber) {
		this.restartNumber = restartNumber;
	}

	public EvolutionStrategyTerminationCondition() {
		this(20);
	}

	@Override
	public boolean shouldTerminate(PopulationData<?> populationData) {
		if (bestFitness < populationData.getBestCandidateFitness()) {
			bestFitness = populationData.getBestCandidateFitness();
			maxCnt = 0;
		} else {
			maxCnt++;
		}
		if (maxCnt == restartNumber)
			return true;
		return false;
	}

}
