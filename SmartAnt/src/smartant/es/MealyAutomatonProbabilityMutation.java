package smartant.es;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import laboratory.plugin.individual.mealy.MealyAutomaton;
import laboratory.plugin.task.ant.Ant;
import laboratory.plugin.task.ant.individual.Automaton;

import org.uncommons.watchmaker.framework.EvolutionaryOperator;

public class MealyAutomatonProbabilityMutation implements
		AdjustableMutator<MealyAutomaton> {

	private double mutationProbability;
	private final double minMutationProbability;
	private final double maxMutationProbability;
	private final double changeEndProbability;
	private final double switchConditionProbability;
	
 	public MealyAutomatonProbabilityMutation(double changeEndProbability,
			double minMutationProbability, double maxMutationProbability,
			double switchConditionProbability) {
		this.changeEndProbability = changeEndProbability;
		this.switchConditionProbability = switchConditionProbability;
		this.minMutationProbability = minMutationProbability;
		this.maxMutationProbability = maxMutationProbability;
		this.mutationProbability = (minMutationProbability + maxMutationProbability) * 0.5;
 	}

	private MealyAutomaton.Transition[][] convertTransitionToMealyTransition(
			Automaton.Transition[][] tr) {
		MealyAutomaton.Transition mtr[][] = new MealyAutomaton.Transition[tr.length][];
		for (int i = 0; i < tr.length; i++) {
			mtr[i] = new MealyAutomaton.Transition[tr[i].length];
			for (int j = 0; j < tr[i].length; j++) {
				mtr[i][j] = (MealyAutomaton.Transition) tr[i][j];
			}
		}
		return mtr;
	}

	private MealyAutomaton applyMulti(MealyAutomaton a, Random rng) {
		MealyAutomaton res = a;
		if (rng.nextDouble() < mutationProbability * 1. / res.getNumberStates()) {
			res = (MealyAutomaton) res.setInitialState(rng.nextInt(res
					.getNumberStates()));
		}
		MealyAutomaton.Transition tr[][] = convertTransitionToMealyTransition(res
				.getTransition());
		for (int i = 0; i < tr.length; i++) {
			for (int j = 0; j < tr.length; j++) {
				if (rng.nextDouble() < mutationProbability) {
					int cause = rng.nextInt(tr[i].length);
					if (rng.nextDouble() < changeEndProbability) {
						tr[i][cause].setEndState(rng.nextInt(tr.length));
					} else {
						if (rng.nextDouble() < switchConditionProbability) {
							int next = (cause + 1) % tr[i].length;
							MealyAutomaton.Transition tmp = tr[i][cause];
							tr[i][cause] = tr[i][next];
							tr[i][next] = tmp;
						} else {

							tr[i][cause] = new MealyAutomaton.Transition(
									tr[i][cause].getEndState(),
									Ant.ACTION_VALUES[rng
											.nextInt(Ant.ACTION_VALUES.length)]);
						}
					}
				}
			}
		}
		res = (MealyAutomaton) res.setTransitions(tr);
		return res;
	}

	@Override
	public List<MealyAutomaton> apply(List<MealyAutomaton> selectedCandidates,
			Random rng) {
		ArrayList<MealyAutomaton> mutated = new ArrayList<MealyAutomaton>();
		for (MealyAutomaton c : selectedCandidates) {
			mutated.add(applyMulti(c, rng));
		}
		return mutated;
	}

	@Override
	public void increaseMutation() {
		if (mutationProbability + 0.05 <= maxMutationProbability) {
			mutationProbability += 0.05;
		}
	}

	@Override
	public void decreaseMutation() {
		if (mutationProbability - 0.05 >= minMutationProbability) {
			mutationProbability -= 0.05;
		}
	}

}
