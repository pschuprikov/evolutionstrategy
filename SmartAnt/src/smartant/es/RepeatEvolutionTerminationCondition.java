package smartant.es;

import java.util.List;

import org.uncommons.watchmaker.framework.termination.GenerationCount;

public class RepeatEvolutionTerminationCondition extends GenerationCount {

	public RepeatEvolutionTerminationCondition(List<?> evolutionHistory) {
		super(evolutionHistory.size());
	}

}
