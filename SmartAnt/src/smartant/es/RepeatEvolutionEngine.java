package smartant.es;

import java.util.List;
import java.util.Random;

import org.uncommons.watchmaker.framework.AbstractEvolutionEngine;
import org.uncommons.watchmaker.framework.CandidateFactory;
import org.uncommons.watchmaker.framework.EvaluatedCandidate;
import org.uncommons.watchmaker.framework.FitnessEvaluator;

public class RepeatEvolutionEngine<T> extends AbstractEvolutionEngine<T> {

	private final List<List<EvaluatedCandidate<T>>> evolutionHistory;
	private int curStep;

	public RepeatEvolutionEngine(CandidateFactory<T> candidateFactory,
			FitnessEvaluator<? super T> fitnessEvaluator, Random rng,
			List<List<EvaluatedCandidate<T>>> evolutionHistory) {
		super(candidateFactory, fitnessEvaluator, rng);
		this.evolutionHistory = evolutionHistory;
		curStep = 0;
	}

	@Override
	protected List<EvaluatedCandidate<T>> nextEvolutionStep(
			List<EvaluatedCandidate<T>> evaluatedPopulation, int eliteCount,
			Random rng) {
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return evolutionHistory.get(curStep++);
	}
}
