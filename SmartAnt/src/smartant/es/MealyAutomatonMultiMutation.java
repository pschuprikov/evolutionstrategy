package smartant.es;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import laboratory.plugin.individual.mealy.MealyAutomaton;
import laboratory.plugin.task.ant.Ant;
import laboratory.plugin.task.ant.individual.Automaton;

import org.uncommons.watchmaker.framework.EvolutionaryOperator;

public class MealyAutomatonMultiMutation implements
		EvolutionaryOperator<MealyAutomaton>, AdjustableMutator<MealyAutomaton> {

	private int mutation;
	private final double changeEndProbability;
	private final double switchConditionProbability;
	private final int minMutation;
	private final Random rng;
	private final int maxMutation;

	public MealyAutomatonMultiMutation(double switchConditionProbability, double changeEndProbability, int minMutation, int maxMutation, Random rng) {
		this.mutation = (minMutation + maxMutation) / 2;
		this.changeEndProbability = changeEndProbability;
		this.minMutation = minMutation;
		this.maxMutation = maxMutation;
		this.rng = rng;
		this.switchConditionProbability = switchConditionProbability;
	}

	private MealyAutomaton.Transition[][] convertTransitionToMealyTransition(
			Automaton.Transition[][] tr) {
		MealyAutomaton.Transition mtr[][] = new MealyAutomaton.Transition[tr.length][];
		for (int i = 0; i < tr.length; i++) {
			mtr[i] = new MealyAutomaton.Transition[tr[i].length];
			for (int j = 0; j < tr[i].length; j++) {
				mtr[i][j] = (MealyAutomaton.Transition) tr[i][j];
			}
		}
		return mtr;
	}

	private MealyAutomaton applyMulti(MealyAutomaton a, Random rng) {
		MealyAutomaton res = a;
		if (rng.nextDouble() < 1. / res.getNumberStates()) {
			res = (MealyAutomaton) res.setInitialState(rng.nextInt(res
					.getNumberStates()));
		}
		MealyAutomaton.Transition tr[][] = convertTransitionToMealyTransition(res
				.getTransition());
		boolean[][][] was = new boolean[3][tr.length][];
		int totalElements = 0;
		for (int i = 0; i < tr.length; i++) {
			for (int j = 0; j < tr[i].length; j++) {
				totalElements++;
			}
		}
		for (int i = 0, ti = 0; i < mutation; i++, ti++) {
			if (ti > totalElements * 2) {
				break;
			}
			int from = rng.nextInt(tr.length);
			if (was[0][from] == null) {
				for (int j = 0; j < 3; j++) {
					was[j][from] = new boolean[tr[from].length];
				}
			}
			int cause = rng.nextInt(tr[from].length);
			if (rng.nextDouble() < changeEndProbability) {
				if (was[0][from][cause]) {
					i--;
					continue;
				}
				was[0][from][cause] = true;
				tr[from][cause].setEndState(rng.nextInt(tr.length));
			} else {
				if (rng.nextDouble() < switchConditionProbability) {
					if (was[1][from][cause]) {
						i--;
						continue;
					}
					was[1][from][cause] = true;
					int next = (cause + 1) % tr[from].length;
					MealyAutomaton.Transition tmp = tr[from][cause];
					tr[from][cause] = tr[from][next];
					tr[from][next] = tmp;
				} else {
					if (was[2][from][cause]) {
						i--;
						continue;
					}
					was[2][from][cause] = true;
					tr[from][cause] = new MealyAutomaton.Transition(
							tr[from][cause].getEndState(),
							Ant.ACTION_VALUES[rng
									.nextInt(Ant.ACTION_VALUES.length)]);
				}
			}
		}
		res = (MealyAutomaton) res.setTransitions(tr);
		return res;
	}

	@Override
	public List<MealyAutomaton> apply(List<MealyAutomaton> selectedCandidates,
			Random rng) {
		ArrayList<MealyAutomaton> mutated = new ArrayList<MealyAutomaton>();
		for (MealyAutomaton c : selectedCandidates) {
			mutated.add(applyMulti(c, rng));
		}
		return mutated;
	}

	/**
	 * Increases number of per automaton mutations
	 * 
	 * @param rng
	 *            random number generator
	 */
	@Override
	public void increaseMutation() {
		if (mutation < maxMutation)
			mutation = mutation + rng.nextInt(maxMutation - mutation + 1);
	}

	/**
	 * Decreases number of per automaton mutations
	 * 
	 * @param rng
	 *            random number generator
	 */
	@Override
	public void decreaseMutation() {
		if (mutation > minMutation)
			mutation = minMutation + rng.nextInt(mutation - minMutation + 1);
	}
}
