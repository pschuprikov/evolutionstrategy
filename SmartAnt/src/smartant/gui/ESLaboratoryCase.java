package smartant.gui;

import javax.swing.JPanel;

public interface ESLaboratoryCase extends Runnable {
	
	void setProperties(ESEngineProperties properties);
	String getCaseName();
	JPanel getPanel();
}
