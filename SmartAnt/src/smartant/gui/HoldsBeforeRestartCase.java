package smartant.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import laboratory.plugin.individual.mealy.MealyAutomaton;
import laboratory.plugin.individual.mealy.factory.MealyAutomatonFactory;
import laboratory.plugin.task.ant.StandardFitness;
import laboratory.plugin.task.ant.individual.Automaton;

import org.uncommons.watchmaker.framework.CandidateFactory;
import org.uncommons.watchmaker.framework.FitnessEvaluator;
import org.uncommons.watchmaker.framework.SelectionStrategy;
import org.uncommons.watchmaker.framework.selection.TruncationSelection;

import smartant.es.EvolutionStrategyMealyAutomatonEngine;
import smartant.es.EvolutionStrategyTerminationCondition;
import smartant.es.MealyAutomatonMultiMutation;

public class HoldsBeforeRestartCase extends AbstractESLaboratoryCase {

	private JPanel propPanel;
	private JPanel casePanel;
	private JLabel numberOfRestartsLabel;
	private JLabel minHoldsLabel;
	private JLabel maxHoldsLabel;
	private JLabel stepHoldsLabel;
	private JSpinner numberOfRestartsSpinner;
	private JSpinner minHoldsSpinner;
	private JSpinner maxHoldsSpinner;
	private JSpinner stepHoldsSpinner;
	private BestShowerPanel bestShowerPanel;
	private final ESHoldsView esHoldsView;
	private final ESHoldsTimeView esHoldsTimeView;

	public HoldsBeforeRestartCase() {
		esHoldsView = new ESHoldsView();
		esHoldsTimeView = new ESHoldsTimeView();
		initGUI();
	}

	private void initGUI() {
		bestShowerPanel = new BestShowerPanel();
		casePanel = new JPanel();
		casePanel.setLayout(new BorderLayout());
		propPanel = new JPanel();
		numberOfRestartsLabel = new JLabel("numberOfRestarts");
		minHoldsLabel = new JLabel("minHolds");
		maxHoldsLabel = new JLabel("maxHolds");
		stepHoldsLabel = new JLabel("stepHolds");
		numberOfRestartsSpinner = new JSpinner(new SpinnerNumberModel(1, 1,
				1000000, 1));
		minHoldsSpinner = new JSpinner(
				new SpinnerNumberModel(10, 1, 100000, 10));
		maxHoldsSpinner = new JSpinner(new SpinnerNumberModel(100, 1, 100000,
				10));
		stepHoldsSpinner = new JSpinner(new SpinnerNumberModel(1, 1, 100000, 1));
		propPanel.add(numberOfRestartsLabel);
		propPanel.add(numberOfRestartsSpinner);
		propPanel.add(minHoldsLabel);
		propPanel.add(minHoldsSpinner);
		propPanel.add(maxHoldsLabel);
		propPanel.add(maxHoldsSpinner);
		propPanel.add(stepHoldsLabel);
		propPanel.add(stepHoldsSpinner);
		casePanel.add(propPanel, BorderLayout.NORTH);
		casePanel.add(bestShowerPanel, BorderLayout.SOUTH);
		JPanel viewHolder = new JPanel(new BorderLayout());
		viewHolder.add(esHoldsTimeView, BorderLayout.WEST);
		viewHolder.add(esHoldsView, BorderLayout.EAST);
		casePanel.add(viewHolder, BorderLayout.CENTER);
	}

	@Override
	public String getCaseName() {
		return "Holds";
	}

	@Override
	public JPanel getPanel() {
		return casePanel;
	}

	public static class BestShowerPanel extends JPanel {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private ArrayList<MealyAutomaton> bests;
		private JButton showBestButton;
		private JSpinner selectBestButton;
		private JLabel maxBestJLabel;
		private final SmartAntRenderer renderer;

		public BestShowerPanel() {
			bests = new ArrayList<MealyAutomaton>();
			renderer = new SmartAntRenderer();
			showBestButton = new JButton("showBest");
			maxBestJLabel = new JLabel("0");
			selectBestButton = new JSpinner(new SpinnerNumberModel(1, 1,
					100000, 1));
			add(maxBestJLabel);
			add(showBestButton);
			add(selectBestButton);
			showBestButton.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					int bestIndex = (Integer) selectBestButton.getValue() - 1;
					if (bestIndex < bests.size()) {
						JFrame frame = new JFrame("best");
						frame.add(renderer.render(bests.get(bestIndex)));
						frame.setSize(800, 800);
						frame.setVisible(true);
					}
				}
			});
		}

		public void setBest(ArrayList<MealyAutomaton> newBests) {
			maxBestJLabel.setText(Integer.valueOf(newBests.size()).toString());
			bests = newBests;
		}

	}

	@Override
	public void run() {

		int numberOfRestatrs = (Integer) numberOfRestartsSpinner.getValue();
		int minHolds = (Integer) minHoldsSpinner.getValue();
		int maxHolds = (Integer) maxHoldsSpinner.getValue();
		int stepHolds = (Integer) stepHoldsSpinner.getValue();
		ArrayList<MealyAutomaton> bestAutomatons = new ArrayList<MealyAutomaton>();
		esHoldsView.setRange(minHolds, maxHolds);
		esHoldsView.clear();
		esHoldsTimeView.clear();
		ESEngineProperties properties = getProperties();
		CandidateFactory<MealyAutomaton> factory = new MealyAutomatonFactory(
				properties.numberOfStates, 0);
		FitnessEvaluator<Automaton> fitness = new StandardFitness<Automaton>();
		SelectionStrategy<Object> selector = new TruncationSelection(0.9);
		double total_time = 0;
		for (int j = minHolds; j <= maxHolds; j += stepHolds) {
			properties.holdsBeforeRestart = j;
			double sum = 0;
			double best = 0;
			MealyAutomaton bestAutomaton = null;
			for (int i = 0; i < numberOfRestatrs; i++) {
				EvolutionStrategyMealyAutomatonEngine evolution = new EvolutionStrategyMealyAutomatonEngine(
						properties, factory, fitness, selector,
						new MealyAutomatonMultiMutation(
								properties.switchConditionProbability,
								properties.changeEndProbability,
								properties.minMutations,
								properties.maxMutations, properties.rng), 100,
						false);
				long start = System.currentTimeMillis();
				MealyAutomaton result = evolution.evolve(10, 0,
						new EvolutionStrategyTerminationCondition(
								properties.holdsBeforeRestart));
				long end = System.currentTimeMillis();
				total_time += (end - start) * 0.001;
				double curFitness = fitness.getFitness(result, null);
				sum += curFitness;
				best = Math.max(curFitness, best);
				if (bestAutomaton == null
						|| fitness.getFitness(bestAutomaton, null) < fitness
								.getFitness(result, null)) {
					bestAutomaton = result;
				}
				if (Thread.currentThread().isInterrupted()) {
					return;
				}
			}
			try {
				esHoldsTimeView.addResult(j, total_time / (numberOfRestatrs));
				esHoldsView.addResult(j, sum / numberOfRestatrs, best);
			} catch (InterruptedException e) {
				return;
			}
			bestAutomatons.add(bestAutomaton);
		}
		File saveDirecrory = new File("/Users/pasa/charts/run_"
				+ new java.util.Date().toLocaleString());
		saveDirecrory.mkdirs();
		esHoldsView.save(saveDirecrory);
		esHoldsTimeView.save(saveDirecrory);
		save(saveDirecrory, numberOfRestatrs);
		bestShowerPanel.setBest(bestAutomatons);
	}

	void save(File directory, int numberOfRestarts) {
		File saveConfigFile = new File(directory.toString() + "/Config.txt");
		try {
			saveConfigFile.createNewFile();
			PrintWriter out = new PrintWriter(saveConfigFile);
			out.println(getProperties().toString());
			out.println("Number of restarts: " + numberOfRestarts);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
