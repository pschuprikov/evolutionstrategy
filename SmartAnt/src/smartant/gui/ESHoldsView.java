package smartant.gui;

import java.awt.BorderLayout;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class ESHoldsView extends JPanel {

	private static final long serialVersionUID = 1L;

	private final XYSeries avgBestSeries;
	private final XYSeries bestBestSeries;
	private final XYSeriesCollection dataset;
	private final JFreeChart chart;
	private final ValueAxis domainAxis;

	public void save(File saveDirectory) {
		try {
			File saveFile = new File(saveDirectory.toString()
					+ "/FitnessSeries.png");
			saveFile.createNewFile();
			ChartUtilities.saveChartAsPNG(saveFile, chart, 600, 400);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public ESHoldsView() {

		setLayout(new BorderLayout());
		avgBestSeries = new XYSeries("average best");
		bestBestSeries = new XYSeries("best of the best");
		dataset = new XYSeriesCollection();
		dataset.addSeries(avgBestSeries);
		dataset.addSeries(bestBestSeries);
		chart = ChartFactory.createXYLineChart("Fitness", "numberOfHolds",
				"fitness", dataset, PlotOrientation.VERTICAL, true, false,
				false);
		domainAxis = chart.getXYPlot().getDomainAxis();
		ChartPanel chartPanel = new ChartPanel(chart);
		add(chartPanel, BorderLayout.CENTER);
	}

	public void setRange(int startHolds, int endHolds) {
		domainAxis.setRange(startHolds, endHolds);
	}

	public void clear() {
		avgBestSeries.clear();
		bestBestSeries.clear();
	}

	public void addResult(final int holds, final double avgFitness,
			final double bestFitness) throws InterruptedException {
		try {
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					avgBestSeries.add(holds, avgFitness);
					bestBestSeries.add(holds, bestFitness);
				}
			});
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
}
