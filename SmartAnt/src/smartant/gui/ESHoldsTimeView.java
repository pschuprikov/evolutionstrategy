package smartant.gui;

import java.awt.BorderLayout;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class ESHoldsTimeView extends JPanel {
	private static final long serialVersionUID = 1L;

	private final XYSeries avgTimeSeries;
	private final XYSeriesCollection dataset;
	private final JFreeChart chart;

	public void save(File saveDirectory) {
		try {
			File saveFile = new File(saveDirectory.toString()
					+ "/TimeSeries.png");	
			saveFile.createNewFile();
			ChartUtilities.saveChartAsPNG(saveFile, chart, 600, 400);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public ESHoldsTimeView() {

		setLayout(new BorderLayout());
		avgTimeSeries = new XYSeries("average Time");
		dataset = new XYSeriesCollection();
		dataset.addSeries(avgTimeSeries);
		chart = ChartFactory.createXYLineChart("Time", "numberOfHolds", "time",
				dataset, PlotOrientation.VERTICAL, true, false, false);
		ChartPanel chartPanel = new ChartPanel(chart);
		add(chartPanel, BorderLayout.CENTER);
	}

	public void clear() {
		avgTimeSeries.clear();
	}

	public void addResult(final int holds, final double time)
			throws InterruptedException {
		try {
			SwingUtilities.invokeAndWait(new Runnable() {
				@Override
				public void run() {
					avgTimeSeries.add(holds, time);
				}
			});
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
}
