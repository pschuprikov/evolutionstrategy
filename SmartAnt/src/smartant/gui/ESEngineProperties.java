package smartant.gui;

import java.util.Random;

public class ESEngineProperties {
	public final Random rng;
	public int minMutations;
	public int maxMutations;
	public int mu;
	public int lambda;
	public int holdsBeforeRestart;
	public int numberOfStates;
	public double switchConditionProbability;
	
	@Override
	public String toString() {
		return "ESEngineProperties [minMutations=" + minMutations
				+ ", maxMutations=" + maxMutations + ", mu=" + mu + ", lambda="
				+ lambda + ", holdsBeforeRestart=" + holdsBeforeRestart
				+ ", numberOfStates=" + numberOfStates
				+ ", switchConditionProbability=" + switchConditionProbability
				+ ", plusStrategy=" + plusStrategy + ", changeEndProbability="
				+ changeEndProbability + "]";
	}
	
	public ESEngineProperties(Random rng, int minMutations, int maxMutations,
			int mu, int lambda, int holdsBeforeRestart, int numberOfStates,
			double switchConditionProbability, boolean plusStrategy,
			double changeEndProbability) {
		super();
		this.rng = rng;
		this.minMutations = minMutations;
		this.maxMutations = maxMutations;
		this.mu = mu;
		this.lambda = lambda;
		this.holdsBeforeRestart = holdsBeforeRestart;
		this.numberOfStates = numberOfStates;
		this.switchConditionProbability = switchConditionProbability;
		this.plusStrategy = plusStrategy;
		this.changeEndProbability = changeEndProbability;
	}
	public final boolean plusStrategy;
	public final double changeEndProbability;
	
}
