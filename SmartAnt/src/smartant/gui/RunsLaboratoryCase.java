package smartant.gui;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import laboratory.plugin.individual.mealy.MealyAutomaton;
import laboratory.plugin.individual.mealy.factory.MealyAutomatonFactory;
import laboratory.plugin.task.ant.StandardFitness;
import laboratory.plugin.task.ant.individual.Automaton;
import laboratory.plugin.visualizator.sAnt.field.MPanel;

import org.uncommons.watchmaker.framework.CandidateFactory;
import org.uncommons.watchmaker.framework.EvaluatedCandidate;
import org.uncommons.watchmaker.framework.FitnessEvaluator;
import org.uncommons.watchmaker.framework.SelectionStrategy;
import org.uncommons.watchmaker.framework.selection.TruncationSelection;

import smartant.es.EvolutionStrategyMealyAutomatonEngine;
import smartant.es.EvolutionStrategyTerminationCondition;
import smartant.es.MealyAutomatonMultiMutation;
import smartant.es.MealyAutomatonProbabilityMutation;
import smartant.es.RepeatEvolutionEngine;
import smartant.es.RepeatEvolutionTerminationCondition;

public class RunsLaboratoryCase extends AbstractESLaboratoryCase {

	private JSpinner numOfRestartsSpinner;
	private JLabel numOfRestartsLabel;
	private JCheckBox saveHistory;
	private ESRunsView esRunsView;
	private JPanel casePanel;

	public RunsLaboratoryCase() {
		esRunsView = new ESRunsView();

		numOfRestartsLabel = new JLabel("numOfRestarts");
		numOfRestartsSpinner = new JSpinner(new SpinnerNumberModel(10, 1,
				1000000, 20));
		casePanel = new JPanel();
		casePanel.setLayout(new BorderLayout());
		JPanel propPanel = new JPanel();
		saveHistory = new JCheckBox("saveHistory", false);
		propPanel.add(numOfRestartsLabel);
		propPanel.add(numOfRestartsSpinner);
		propPanel.add(saveHistory);
		casePanel.add(propPanel, BorderLayout.NORTH);
		casePanel.add(esRunsView, BorderLayout.CENTER);
	}

	@Override
	public void run() {
		int numberOfRestatrs = (Integer) numOfRestartsSpinner.getValue();
		esRunsView.setMaxRuns(numberOfRestatrs);
		Automaton bestResult = null;
		ESEngineProperties properties = getProperties();
		List<List<EvaluatedCandidate<MealyAutomaton>>> bestEvolutionHistory = null;
		CandidateFactory<MealyAutomaton> factory = new MealyAutomatonFactory(
				properties.numberOfStates, 0);
		FitnessEvaluator<Automaton> fitness = new StandardFitness<Automaton>();
		SelectionStrategy<Object> selector = new TruncationSelection(0.9);
		boolean useHistory = saveHistory.isSelected();
		for (int i = 0; i < numberOfRestatrs; i++) {
			EvolutionStrategyMealyAutomatonEngine evolution = new EvolutionStrategyMealyAutomatonEngine(
					properties, factory, fitness, selector,
					// new MealyAutomatonMultiMutation(
					// properties.switchConditionProbability,
					// properties.changeEndProbability,
					// properties.minMutations, properties.maxMutations,
					// properties.rng),
					new MealyAutomatonProbabilityMutation(
							properties.changeEndProbability,
							properties.minMutations * 0.01,
							properties.maxMutations * 0.01,
							properties.switchConditionProbability),
					properties.holdsBeforeRestart <= 200 ? 1 : 100, useHistory);
			evolution.setSingleThreaded(true);
			Automaton result = evolution.evolve(10, 0,
					new EvolutionStrategyTerminationCondition(
							properties.holdsBeforeRestart));
			try {
				if (i % Math.max(numberOfRestatrs / 1000, 1) == 0)
					esRunsView.addResult(i, fitness.getFitness(result, null));
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			if (bestResult == null
					|| fitness.getFitness(result, null) > fitness.getFitness(
							bestResult, null)) {
				bestResult = result;
				bestEvolutionHistory = evolution.getHistory();
			}
			if (Thread.currentThread().isInterrupted()) {
				return;
			}
		}
		if (useHistory) {
			List<List<EvaluatedCandidate<MealyAutomaton>>> reducedHistory = new ArrayList<List<EvaluatedCandidate<MealyAutomaton>>>();
			for (int i = 0; i < bestEvolutionHistory.size(); i += Math.max(
					bestEvolutionHistory.size() / 1000, 1)) {
				reducedHistory.add(bestEvolutionHistory.get(i));
			}
			RepeatEvolutionEngine<MealyAutomaton> repeater = new RepeatEvolutionEngine<MealyAutomaton>(
					factory, fitness, properties.rng, reducedHistory);
			repeater.setSingleThreaded(true);
			repeater.addEvolutionObserver(getMonitor());
			repeater.evolve(10, 0, new RepeatEvolutionTerminationCondition(
					reducedHistory));
			getMonitor().showInFrame("Best evolution", false);
		} else {
			new SmartAntRenderer().render(bestResult).setVisible(true);
		}
	}

	@Override
	public JPanel getPanel() {
		return casePanel;
	}

	@Override
	public String getCaseName() {
		return "Runs";
	}

}
