package smartant.gui;

import laboratory.plugin.individual.mealy.MealyAutomaton;

import org.uncommons.watchmaker.swing.evolutionmonitor.EvolutionMonitor;

public abstract class AbstractESLaboratoryCase implements ESLaboratoryCase {

	private ESEngineProperties properties;
	private final EvolutionMonitor<MealyAutomaton> evolutionMonitor;
	private final SmartAntRenderer renderer;
	
	public AbstractESLaboratoryCase() {
		renderer = new SmartAntRenderer();
		evolutionMonitor = new EvolutionMonitor<MealyAutomaton>(renderer, false);
	}
	
	protected final EvolutionMonitor<MealyAutomaton> getMonitor() {
		return evolutionMonitor;
	}
	
	@Override
	public final void setProperties(ESEngineProperties properties) {
		this.properties = properties;
	}
	
	protected final ESEngineProperties getProperties() {
		return properties;
	}
}
