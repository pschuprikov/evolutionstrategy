package smartant.gui;

import javax.swing.JComponent;

import laboratory.plugin.task.ant.individual.Automaton;
import laboratory.plugin.visualizator.sAnt.field.Field;

import org.uncommons.watchmaker.framework.interactive.Renderer;

public class SmartAntRenderer implements Renderer<Automaton, JComponent> {

	@Override
	public JComponent render(Automaton entity) {
		return new Field("Ololo", entity, 630, 490, 0.5, 32, 10);
	}

}
